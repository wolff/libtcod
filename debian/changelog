libtcod (1.18.1+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-* and Files-Excluded fields.
  * Upgrade to Standards-Version 4.6.0 and debhelper compat level 13.
  * Update debian/rules.
  * Update 00-fix-makefile.patch.
  * Update years in debian/copyright.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sat, 11 Sep 2021 18:03:34 +0200

libtcod (1.14.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Delete 01-printf-fmt.patch, 02-link-pthread.patch and
    03-no-python-2.patch (all fixed upstream).

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sat, 14 Sep 2019 14:41:48 +0200

libtcod (1.13.0+dfsg-1) unstable; urgency=medium

  * Add +dfsg to version number, which I forgot in the previous upload.
  * Add 03-no-python-2.patch, and drop python2 as a build dependency
    (Closes: #936925).
  * Fix autopkgtests by adding pkg-config test dependency.
  * Copy full text of the BSD-3-clause license into debian/copyright in
    order to fix the copyright-refers-to-deprecated-bsd-license-file
    Lintian tag.
  * Add dependency on libsdl2-dev to libtcod-dev.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sun, 01 Sep 2019 16:35:20 +0200

libtcod (1.13.0-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/watch file.
  * Upgrade to Standards-Version 4.4.0 (no changes).
  * Update Homepage field in debian/control.
  * Update patches:
     - Update 00-fix-makefile.patch.
     - Remove 01-python-multiarch.patch (no longer relevant, see below).
     - Remove 02-vsnwprintf.patch (fixed upstream).
     - Add patch 01-printf-fmt.patch and 02-link-pthread.patch.
  * Change binary package name to match new SONAME.
  * Update debian/copyright file.
  * Upgrade to debhelper compat version 12.
  * Install pkg-config file for libtcod.
  * Update debian/tests/.
  * Drop Python bindings (no longer maintained upstream; throws a
    DeprecationWarning when imported in Python).

 -- Fabian Wolff <fabi.wolff@arcor.de>  Tue, 27 Aug 2019 17:56:10 +0200

libtcod (1.7.0+dfsg-2) unstable; urgency=medium

  * Add 02-vsnwprintf.patch to fix build failures on ppc*/powerpc*
    platforms caused by an undefined reference to `vsnwprintf'
    (Closes: #901859).

 -- Fabian Wolff <fabi.wolff@arcor.de>  Thu, 05 Jul 2018 12:22:52 +0200

libtcod (1.7.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/copyright.
  * Update patches:
     - Refresh 00-fix-makefile.patch.
     - Remove 01-fix-cppcheck.patch (fixed upstream).
     - Rename (and refresh) patch 02-python-multiarch.patch to
       01-python-multiarch.patch.
     - Remove 03-include-files.patch (fixed upstream).
  * Update debian/rules.
  * Update symbols file.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sun, 17 Jun 2018 21:33:59 +0200

libtcod (1.6.7+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Update debian/rules (filename of upstream changelog has changed).

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sat, 19 May 2018 21:16:24 +0200

libtcod (1.6.6+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update patches:
     - Refresh 00-fix-makefile.patch.
     - Remove 03-disable-rexpaint.patch (fixed upstream).
     - Remove 04-include-pstdint.h.patch (fixed upstream).
     - Add 03-include-files.patch.
  * Update symbols file.
  * Upgrade to Standards-Version 4.1.4 in debian/control (no changes).
  * Update debian/copyright.
  * Add python3-libtcod package.
  * Restrict the libtcod0 and libtcod-dev packages to linux-any.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Tue, 01 May 2018 18:29:02 +0200

libtcod (1.6.5+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update 03-disable-rexpaint.patch.
  * Add Vcs-Git and Vcs-Browser fields in debian/control.
  * Upgrade to Standards-Version 4.1.3 in debian/control (no changes).
  * Add 04-include-pstdint.h.patch (Closes: #892053).
  * Upgrade to debhelper compat level 11 (no changes).
  * Update debian/copyright.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sun, 25 Mar 2018 21:00:54 +0200

libtcod (1.6.4+dfsg-2) unstable; urgency=medium

  * Add patch 03-disable-rexpaint.patch to remove support for loading
    and saving REXPaint files (Closes: #884000).

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sun, 24 Dec 2017 17:59:10 +0100

libtcod (1.6.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Add Files-Excluded field in debian/copyright.
  * Refresh patches.
  * Upgrade to Standards-Version 4.1.2:
    debian/control: change priority from "extra" to "optional"
  * Update symbols file.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sat, 09 Dec 2017 15:48:50 +0100

libtcod (1.6.3+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Upgrade to Standards-Version 4.0.0 (no changes).
  * Upgrade to debhelper compat level 10.
  * Update symbols file.
  * Update debian/copyright.
  * Refresh patches.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Wed, 21 Jun 2017 16:50:29 +0200

libtcod (1.6.1+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Fix debian/watch.
  * Update patches:
     - Remove 0001-Use-global-zlib.h.patch (fixed upstream).
     - Rewrite 0002-Fix-soname.patch as 00-fix-makefile.patch.
     - Remove 0003-Fix-spelling-errors.patch (fixed upstream).
     - Remove 0005-Fix-warnings.patch (fixed upstream).
     - Rename patch 0004-Fix-cppcheck.patch as 01-fix-cppcheck.patch.
  * Update symbols file.
  * Adjust debian/rules.
  * Update debian/copyright.
  * Install upstream changelog.
  * Add the python-libtcod package.
  * Add patch 02-python-multiarch.patch to help the Python wrapper find
    libtcod.so in /usr/lib/<MULTIARCH>/.
  * Add debian/clean file and override_dh_auto_clean target in debian/rules.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Fri, 21 Oct 2016 18:59:46 +0200

libtcod (1.6.0~pre1+dfsg-1) unstable; urgency=medium

  * Initial release. Closes: #704587

 -- Fabian Wolff <fabi.wolff@arcor.de>  Tue, 26 Apr 2016 21:19:14 +0200
